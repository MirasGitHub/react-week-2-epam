import "./App.css";
import Page from "./components/Page";
import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";

function App() {
	return (
		<div className="App">
			<Page>
				<Header title="Welcome to my React App" />
				<Main />
				<Footer title="Footer" />
			</Page>
		</div>
	);
}

export default App;
