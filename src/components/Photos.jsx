import PropTypes from "prop-types";

const Photos = ({ data }) => {
	const firstTenPhotos = data.slice(0, 10);

	const photos = firstTenPhotos.map((photo) => (
		<div key={photo.id}>
			<h5>{photo.title}</h5>
			<img src={photo.url} alt={photo.title} />
		</div>
	));

	return <>{photos}</>;
};

export default Photos;

Photos.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object),
	id: PropTypes.number,
	title: PropTypes.string,
	url: PropTypes.string,
};
