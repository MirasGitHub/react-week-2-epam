import React from "react";
import Photos from "./Photos";
import PropTypes from "prop-types";
import { data } from "../data/data";

const Main = () => {
	return (
		<React.Fragment>
			<Photos data={data} />
		</React.Fragment>
	);
};

export default Main;

Photos.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object),
};
