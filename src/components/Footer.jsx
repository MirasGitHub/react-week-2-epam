import PropTypes from "prop-types";

const Footer = ({ title = "Footer title" }) => {
	return (
		<div className="footer">
			<small>
				<p>{title}</p>
			</small>
			<small>
				<p>©Copyright 2023!</p>
			</small>
		</div>
	);
};

Footer.propTypes = {
	title: PropTypes.string,
};

export default Footer;
